%% The main initialization script

% Room parameters initialization
% All the parameters related to a room are collected in the specific
% structures room201 and room202 with the fields C, R_external, R_internal, and R_interroom.

[room201, room202] = getRoomParameters();

% The external temperature
[T_external] = getExternalTemperatureRecords();

% We model the rest of the building as a constant temperature.
T_internal = 16;

% Heating subsystem parameters
% Given 2x2kW power source, the maximum heating in 4kW
P_heating_max_W = 4000;

% Ocuppants
% Each ocuppant produces 80 W
heat_by_person = 80;

% load the file if no Coder is installed. Otherwise, use the embedded
% function.
load('occuppancy_data_room201.mat');
% max prsons in the room
persons_max = 30;
% distribution data has two collumns: time stamps and the probability of a
% person to be present.
distribution_data = createPresenceProbabilty();


function [room201,room202] = getRoomParameters()
%GETROOMPARAMETERS Computes RC model parameters for two rooms

% common parameters
parallelConnection = @(varargin)1/sum(1./[varargin{:}]); %Parallel function for resistors

R_EW1 = 6.6/3600;   % m^2*K*h/J
R_EW2 = 8.25/3600;  % m^2*K*h/J
R_IW1 = 0.72/3600;  % m^2*K*h/J 
R_IW2 = 1.88/3600;  % m^2*K*h/J
R_F1 = 1.15/3600;   % m^2*K*h/J
R_R1 = 7.25/3600;   % m^2*K*h/J
R_W1 = 0.26/3600;   % m^2*K*h/J
R_W2 = 0.28/3600;   % m^2*K*h/J
R_W3 = 0.27/3600;   % m^2*K*h/J
R_W4 = 0.35/3600;   % m^2*K*h/J
R_D1 = 0.56/3600;   % m^2*K*h/J
R_D2 = 0.52/3600;   % m^2*K*h/J

%% Room 201

C_1 = 308360; % J/K

R_NE = parallelConnection(R_EW1/21.82,R_EW2/2.07,R_W3/4.28);
R_NW = parallelConnection(R_EW1/15.58,R_EW2/10.3,R_W1/6.7,R_W2/13.39);
R_SE = parallelConnection(R_IW2/41.93,R_D2/1.84);
R_SW = parallelConnection(R_IW1/23.16,R_W4/3.88,R_D1/2.86);

R_F = R_F1/90.64;
R_R = R_R1/90.64;

R_eq_out = parallelConnection(R_NE,R_NW, R_R);
R_eq_room_prime = parallelConnection(R_SE,R_F);
R_eq_corridor = R_SW;

room201 = struct(...
            'C', C_1,...
            'R_external', R_eq_out,...
            'R_internal', R_eq_corridor,...
            'R_interroom', R_eq_room_prime);

%% Second Room

C_2 = 308360;
R_NE_2 = parallelConnection(R_EW1/21.82,R_EW2/2.07,R_W3/4.28);
R_NW_2 = parallelConnection(R_D2/1.84,R_IW2/41.93);
R_SE_2 = parallelConnection(R_EW1/15.58,R_EW2/10.3,R_W1/6.7,R_W2/13.4);
R_SW_2 = parallelConnection(R_EW1/9.83,R_EW2/3.24,R_W1/3.35,R_W2/3.35);
R_F_2 = R_F1/90.64;
R_R_2 = R_R1/90.64;
R_eq_out_2 = parallelConnection(R_NE_2,R_SE_2,R_R_2,R_SW_2);
R_eq_room_prime_2 = parallelConnection(R_NW_2,R_F_2);
R_eq_corridor_2 = parallelConnection(R_D1/2.86,R_IW1/5.98);

room202 = struct(...
            'C', C_2,...
            'R_external', R_eq_out_2,...
            'R_internal', R_eq_corridor_2,...
            'R_interroom', R_eq_room_prime_2);


end

function [T_external_data] = getExternalTemperatureRecords()
    T_external_monday = [4 2 1 1 1 0 0 2 5 7 8 10 11 12 12 12 12 11 9 7 6 5 4 4];
    T_external_tuesday = [3 3 3 3 2 1 1 3 5 8 9 10 11 12 13 13 13 12 9 7 6 5 4 4];
    T_external_wednesday = [4 2 1 1 1 0 0 1 4 6 8 10 10 11 11 12 11 11 9 7 6 6 6 5];
    T_external_thursday = [4 4 3 3 1 1 1 2 5 7 9 10 11 12 12 12 12 12 9 7 6 6 5 5];
    T_external_friday = [5 4 4 3 2 1 1 2 5 6 7 9 11 12 13 12 12 11 10 9 7 6 6 5];
    T_external_saturday = [5 5 3 3 1 1 2 2 5 6 9 10 10 11 11 12 12 12 10 8 6 6 5 5];
    T_external_sunday = [4 4 3 2 1 0 0 1 3 5 6 8 10 12 12 12 11 10 9 7 6 5 4 4];

    T_external_week= [T_external_monday T_external_tuesday T_external_wednesday T_external_thursday T_external_friday T_external_saturday T_external_sunday]';

    T_external_time = (1:length(T_external_week))';

    T_external_data = [T_external_time, T_external_week];
end

function distribution_data = createPresenceProbabilty()

    % for 7*24*60 minutes
    distribution_data = zeros(7*24*60,2);
    distribution_data(:,1) = (1:7*24*60)'/60; %in hours
    
    % one-day distribution
    P = zeros(1440, 1);

        % From 00h to 7h we the probability of having someone in the room is low.
        for i = 1:420
            P(i) = 0.001;
        end
        % From 7h to 8h30 we the probability rises linearly until we reach 95%.
        for i = 421:510
            P(i) = P(i-1) + 0.0106;
        end
        % The probability stays the same until 10h
        for i = 511:600
            P(i) = P(i-1);
        end
        % From 10h to 10h15 the probability decreases linearly until we reach 50%.
        for i = 600:615
            P(i) = P(i-1) - 0.031;
        end
        % From 10h15 to 10h30 the probability rises linearly until we reach 95%.
        for i = 616:631
            P(i) = P(i-1) + 0.031;
        end
        % The probability stays the same until 12h
        for i = 632:720
            P(i) = P(i-1);
        end
        % From 12h to 12h30 the probability decreases linearly until we reach 2.5%
        for i = 720:750
            P(i) = P(i-1) - 0.03;
        end
        % From 12h30 to 13h the probability stays the same
        for i = 751:780
            P(i) = P(i-1);
        end
        % From 13h to 13h30 the probability rises linearly until we reach 95%.
        for i = 781:811
            P(i) = P(i-1) + 0.03;
        end
        % From 13h30 to 15h the probability stays the same
        for i = 812:900
            P(i) = P(i-1);
        end
        % From 15h to 15h15 the probability decreases linearly until we reach 50%.
        for i = 901:915
            P(i) = P(i-1) - 0.031;
        end
        % From 15h15 to 15h30 the probability rises linearly until we reach 95%.
        for i = 916:930
            P(i) = P(i-1) + 0.031;
        end
        % The probability stays the same until 17h
        for i = 931:1030
            P(i) = P(i-1);
        end
        % From 17h to 19h the probability decreases linearly until we reach 0.7%
        for i = 1031:1150
            P(i) = P(i-1) - 0.0079;
        end
        % From 19h to 23h59 the probability decreases linearly until we reach 0.7%
        for i = 1151:1440
            P(i) = P(i-1);
        end
    
    % repeat the one-day for 5 days
    P_working_days = repmat(P,5,1);
    
    P_weekends = zeros(2*24*60,1);
    
    distribution_data(:,2) = [P_working_days; P_weekends];    
    
end

